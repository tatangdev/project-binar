const {output} = require('./output');
const {input, rl} = require('./question');

const daftarNilai = [];
let quit = false;

const main = async () => {
  while(!quit){
    const nilaiSiswa = await input(">>> ")
    if(nilaiSiswa == 'q' || nilaiSiswa == 'Q'){
      quit = true
    }else{
      daftarNilai.push(+nilaiSiswa);
    }
  }
  rl.close()
  output(daftarNilai)
}

main();